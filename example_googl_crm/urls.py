# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, re_path, include
from rest_framework import routers
from rest_framework.authtoken import views

from googl_crm.views import GoogleMailViewSet
from .views import ContactDetailView, DashView, HomeView, SettingsView


router = routers.DefaultRouter()
router.register(r"googlemail", GoogleMailViewSet, basename="googlemail")

urlpatterns = [
    re_path(r"^", view=include("login.urls")),
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^api/0.1/", view=include((router.urls, "api"), namespace="api")),
    re_path(
        r"^contact/(?P<pk>\d+)/$",
        view=ContactDetailView.as_view(),
        name="contact.detail",
    ),
    re_path(r"^crm/", view=include("crm.urls")),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(r"^mail/", view=include("mail.urls")),
    re_path(r"^googl/", view=include("googl.urls")),
    re_path(r"^googl_crm/", view=include("googl_crm.urls")),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^token/$", view=views.obtain_auth_token, name="api.token.auth"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
