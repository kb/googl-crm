# -*- encoding: utf-8 -*-
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.db import models
from django.db.models import Count, Q
from django.utils import timezone

from base.model_utils import TimedCreateModifyDeleteModel
from crm.models import Ticket
from crm.service import get_contact_model
from googl.models import GoogleMail


class MailTicketManager(models.Manager):
    def create_mail_ticket(self, mail, ticket, user):
        x = self.model(mail=mail, ticket=ticket, user=user)
        x.save()
        return x

    def init_mail_ticket(self, mail, ticket, user):
        try:
            x = self.model.objects.get(mail=mail, ticket=ticket)
            if x.is_deleted:
                x.undelete()
        except self.model.DoesNotExist:
            x = self.create_mail_ticket(mail, ticket, user)
        return x

    def notify_new_emails(self):
        """Are there any new tickets for any contact?

        Check the following:

        1. New Google Mail messages (``googl.GoogleMail``).
        2. Messages which are not assigned to a ticket (``MailTicket``).
        3. Contact email addresses (``contact.ContactEmail``).

        .. note:: We only notify new email addresses for contacts.  We are not
                  worried about other emails because they are probably not
                  urgent.

        """
        from_date_time = timezone.now() + relativedelta(hours=-2)
        qs = GoogleMail.objects.filter(email_date__gte=from_date_time)
        # exclude emails which have already been assigned to a ticket
        mail_pks = [x.pk for x in qs]
        mail_ticket_qs = MailTicket.objects.filter(mail__pk__in=mail_pks)
        exclude_pks = [x.mail.pk for x in mail_ticket_qs]
        # only include emails from a contact on our database
        emails = [x.from_email for x in qs]
        contact_emails = get_contact_model().objects.contact_emails(emails)
        return (
            qs.filter(from_email__in=contact_emails)
            .exclude(pk__in=exclude_pks)
            .order_by("-email_date")
        )

    def ticket_count(self):
        """Return a list of mail messages annotated with the ticket count."""
        return (
            GoogleMail.objects.all()
            .order_by("-email_date", "msg_id")
            .annotate(
                ticket_count=Count(
                    "mailticket", filter=Q(mailticket__deleted=False)
                )
            )
        )


class MailTicket(TimedCreateModifyDeleteModel):
    """Link a (Google) mail message to a ticket."""

    mail = models.ForeignKey(GoogleMail, on_delete=models.CASCADE)
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    objects = MailTicketManager()

    class Meta:
        ordering = ("mail__email_date",)
        unique_together = ("mail", "ticket")
        verbose_name = "Ticket mail"
        verbose_name_plural = "Ticket mail"

    def __str__(self):
        return "{} - {}".format(self.ticket.title, self.mail.subject)

    @property
    def description(self):
        """For the 'ticket_activity' function (see 'invoice/models.py'."""
        return "From {} to {}\n{}".format(
            self.mail.from_email, self.mail.to_email, self.mail.summary()
        )

    @property
    def title(self):
        """For the 'ticket_activity' function (see 'invoice/models.py'."""
        return self.mail.subject
