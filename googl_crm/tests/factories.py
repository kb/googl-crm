# -*- encoding: utf-8 -*-
import factory

from crm.tests.factories import TicketFactory
from googl.tests.factories import GoogleMailFactory
from googl_crm.models import MailTicket
from login.tests.factories import UserFactory


class MailTicketFactory(factory.django.DjangoModelFactory):
    mail = factory.SubFactory(GoogleMailFactory)
    ticket = factory.SubFactory(TicketFactory)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = MailTicket
