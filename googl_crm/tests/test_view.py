# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from contact.tests.factories import ContactEmailFactory, ContactFactory
from crm.tests.factories import TicketFactory
from googl.tests.factories import GoogleMailFactory
from googl_crm.models import MailTicket
from login.tests.factories import TEST_PASSWORD, UserFactory
from .factories import MailTicketFactory


@pytest.mark.django_db
def test_mail_ticket_update(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(title="c1", user=UserFactory(username="z"))
    ContactEmailFactory(contact=contact, email="patrick@kbsoftware.co.uk")
    t1 = TicketFactory(contact=contact, title="t1")
    TicketFactory(contact=contact, title="t2")
    t3 = TicketFactory(contact=contact, title="t3")
    # match contacts using this email address
    mail = GoogleMailFactory(from_email="patrick@kbsoftware.co.uk")
    url = reverse("googl_crm.mail.ticket.create", args=[mail.pk])
    response = client.post(url, data={"many_to_many": [t1.pk, t3.pk]})
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("googl_crm.mail.list") == response.url
    assert set([t1.pk, t3.pk]) == set(
        [x.ticket.pk for x in MailTicket.objects.filter(mail=mail)]
    )


@pytest.mark.django_db
def test_mail_ticket_update_get(client):
    """Check the initial list of tickets is correct."""
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact_1 = ContactFactory(title="c1", user=UserFactory(username="z"))
    ContactEmailFactory(contact=contact_1, email="patrick@kbsoftware.co.uk")
    TicketFactory(contact=contact_1, title="t1")
    # complete
    ticket = TicketFactory(contact=contact_1, title="t2")
    ticket.set_complete(user)
    ticket.save()
    # different contact
    TicketFactory(contact=ContactFactory(), title="t3")
    t4 = TicketFactory(contact=contact_1, title="t4")
    # different contact, but same email address
    contact_2 = ContactFactory(title="c2", user=UserFactory(username="a"))
    ContactEmailFactory(contact=contact_2, email="patrick@kbsoftware.co.uk")
    TicketFactory(contact=contact_2, title="t5")
    # match contacts using this email address
    mail = GoogleMailFactory(from_email="patrick@kbsoftware.co.uk")
    MailTicketFactory(mail=mail, ticket=t4)
    url = reverse("googl_crm.mail.ticket.create", args=[mail.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    many_to_many = form.fields["many_to_many"]
    assert ["t5", "t4", "t1"] == [x.title for x in many_to_many.queryset]
    assert {t4.pk: True} == many_to_many.initial
    assert "mail" in response.context
    assert mail == response.context["mail"]
    assert "contacts" in response.context
    contacts = response.context["contacts"]
    assert set(["c1", "c2"]) == set([x.title for x in contacts])
