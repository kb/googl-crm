# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from googl.tests.factories import GoogleMailFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_mail_list(perm_check):
    url = reverse("googl_crm.mail.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_mail_ticket_update(perm_check):
    mail = GoogleMailFactory()
    url = reverse("googl_crm.mail.ticket.create", args=[mail.pk])
    perm_check.staff(url)
