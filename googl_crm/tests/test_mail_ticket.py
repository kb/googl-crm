# -*- encoding: utf-8 -*-
import pytest

from crm.tests.factories import TicketFactory
from googl.tests.factories import GoogleMailFactory
from googl_crm.models import MailTicket
from login.tests.factories import UserFactory
from .factories import MailTicketFactory


@pytest.mark.django_db
def test_str():
    mail = GoogleMailFactory(subject="Fruit")
    ticket = TicketFactory(title="Apple")
    assert "Apple - Fruit" == str(MailTicketFactory(ticket=ticket, mail=mail))


@pytest.mark.django_db
def test_init_mail_ticket():
    mail = GoogleMailFactory()
    ticket = TicketFactory()
    user = UserFactory()
    mail_ticket = MailTicket.objects.init_mail_ticket(mail, ticket, user)
    mail_ticket.refresh_from_db()
    assert mail == mail_ticket.mail
    assert ticket == mail_ticket.ticket
    assert user == mail_ticket.user


@pytest.mark.django_db
def test_init_mail_ticket_deleted():
    mail = GoogleMailFactory()
    ticket = TicketFactory()
    user = UserFactory()
    mail_ticket = MailTicket.objects.init_mail_ticket(mail, ticket, user)
    mail_ticket.refresh_from_db()
    assert mail_ticket.is_deleted is False
    mail_ticket.set_deleted(user)
    mail_ticket.refresh_from_db()
    assert mail_ticket.is_deleted is True
    mail_ticket = MailTicket.objects.init_mail_ticket(mail, ticket, user)
    mail_ticket.refresh_from_db()
    assert mail_ticket.is_deleted is False


@pytest.mark.django_db
def test_ticket_count():
    m1 = GoogleMailFactory(msg_id="m1")
    GoogleMailFactory(msg_id="m2")
    m3 = GoogleMailFactory(msg_id="m3")
    MailTicketFactory(mail=m1)
    MailTicketFactory(mail=m1)
    MailTicketFactory(mail=m3)
    qs = MailTicket.objects.ticket_count()
    assert {"m1": 2, "m2": 0, "m3": 1} == {x.msg_id: x.ticket_count for x in qs}
