# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import datetime
from django.urls import reverse
from freezegun import freeze_time
from http import HTTPStatus

from api.tests.fixture import api_client
from contact.tests.factories import ContactEmailFactory
from googl_crm.tests.factories import MailTicketFactory
from googl.tests.factories import GoogleMailFactory


@pytest.mark.django_db
def test_google_mail(api_client):
    frozen_datetime = datetime(2017, 5, 21, 16, 10, 0, tzinfo=pytz.utc)
    with freeze_time(frozen_datetime):
        # include
        ContactEmailFactory(email="a@a.com")
        GoogleMailFactory(subject="a", from_email="a@a.com")
        # more than two hours ago
        ContactEmailFactory(email="b@b.com")
        GoogleMailFactory(
            subject="b",
            from_email="b@b.com",
            email_date=datetime(2017, 5, 21, 14, 1, 0, tzinfo=pytz.utc),
        )
        # less than two hours ago
        ContactEmailFactory(email="c@c.com")
        GoogleMailFactory(
            subject="c",
            from_email="c@c.com",
            email_date=datetime(2017, 5, 21, 15, 10, 0, tzinfo=pytz.utc),
        )
        # already assigned
        ContactEmailFactory(email="d@d.com")
        mail = GoogleMailFactory(subject="d", from_email="d@d.com")
        MailTicketFactory(mail=mail)
        # not a contact email
        GoogleMailFactory(subject="e")
        # include
        ContactEmailFactory(email="f@f.com")
        GoogleMailFactory(subject="f", from_email="f@f.com")
        # test
        response = api_client.get(
            reverse("api:googlemail-list"), content_type="application/json"
        )
        assert HTTPStatus.OK == response.status_code, response.data
        assert [
            {"from_email": "a@a.com"},
            {"from_email": "f@f.com"},
            {"from_email": "c@c.com"},
        ] == response.json()
