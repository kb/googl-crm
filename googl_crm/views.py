# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView, ListView
from rest_framework import authentication, permissions, viewsets

from base.view_utils import BaseMixin
from crm.service import get_contact_model
from googl.models import GoogleMail
from googl.serializers import GoogleMailSerializer
from googl_crm.models import MailTicket

from .forms import MailTicketForm


class GoogleMailViewSet(viewsets.ModelViewSet):
    """

    I will design the API call so that we check for new emails every 30 minutes.

    Discussed with Andrea.  If a client has an issue which is stopping them
    working, then:

    1. A half hour response time would be good.
    2. An hour would probably be a bit too long.

    """

    authentication_classes = (
        authentication.TokenAuthentication,
        authentication.SessionAuthentication,
    )
    http_method_names = ["get"]
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = GoogleMailSerializer

    def get_queryset(self):
        return MailTicket.objects.notify_new_emails()


class MailTicketListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20
    template_name = "crm/google_mail_ticket_count_list.html"

    def get_queryset(self):
        return MailTicket.objects.ticket_count()


class MailTicketCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):

    model = MailTicket
    form_class = MailTicketForm

    def _contacts(self, mail):
        return get_contact_model().objects.matching_email(mail.from_email)

    def _mail(self):
        pk = self.kwargs["mail_pk"]
        return GoogleMail.objects.get(pk=pk)

    def form_valid(self, form):
        mail = self._mail()
        self.object = form.save(commit=False)
        many_to_many = form.cleaned_data["many_to_many"]
        with transaction.atomic():
            # mark all existing records as deleted
            pks = [
                x.pk
                for x in MailTicket.objects.filter(mail=mail).exclude(
                    deleted=True
                )
            ]
            for pk in pks:
                mail_ticket = MailTicket.objects.get(pk=pk)
                mail_ticket.set_deleted(self.request.user)
            # update
            for ticket in many_to_many:
                MailTicket.objects.init_mail_ticket(
                    mail, ticket, self.request.user
                )
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        mail = self._mail()
        context.update(dict(contacts=self._contacts(mail), mail=mail))
        return context

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        mail = self._mail()
        contact_qs = self._contacts(mail)
        qs = MailTicket.objects.filter(mail=mail).exclude(deleted=True)
        result.update(
            dict(
                contact_pks=[x.pk for x in contact_qs],
                ticket_pks=[x.ticket.pk for x in qs],
            )
        )
        return result

    def get_success_url(self):
        return reverse("googl_crm.mail.list")
