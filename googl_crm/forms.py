# -*- encoding: utf-8 -*-
from django import forms
from django.urls import reverse
from django.utils.html import format_html

from crm.models import Ticket
from .models import MailTicket


def _label_from_many_to_many_instance(x):
    url = reverse("crm.ticket.detail", args=[x.pk])
    return format_html(
        "{} <a href='{}'>{}</a><small>({})</small>".format(
            x.number, url, x.title, x.contact.full_name
        )
    )


class ManyToManyMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return _label_from_many_to_many_instance(obj)


class MailTicketForm(forms.ModelForm):

    many_to_many = ManyToManyMultipleChoiceField(
        queryset=Ticket.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
    )

    class Meta:
        model = MailTicket
        fields = ("many_to_many",)

    def __init__(self, *args, **kwargs):
        contact_pks = kwargs.pop("contact_pks")
        ticket_pks = kwargs.pop("ticket_pks")
        super().__init__(*args, **kwargs)
        qs = (
            Ticket.objects.current()
            .filter(contact__pk__in=contact_pks)
            .order_by("contact__user__username", "-pk")
        )
        f = self.fields["many_to_many"]
        f.queryset = qs
        f.label = ""
        f.initial = {x: True for x in ticket_pks}
