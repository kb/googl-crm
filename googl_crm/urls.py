# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import MailTicketCreateView, MailTicketListView

urlpatterns = [
    re_path(
        r"^mail/$",
        view=MailTicketListView.as_view(),
        name="googl_crm.mail.list",
    ),
    re_path(
        r"^mail/(?P<mail_pk>\d+)/ticket/$",
        view=MailTicketCreateView.as_view(),
        name="googl_crm.mail.ticket.create",
    ),
]
