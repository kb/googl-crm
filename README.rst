Google and CRM
**************

Install
=======

Virtual Environment
-------------------

::

  python3 -m venv venv-googl-crm
  source venv-googl-crm/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
